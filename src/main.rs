// for argument parsing
extern crate clap;
use clap::{App, Arg, ArgMatches};

#[macro_use]
extern crate scan_fmt;

// for error handling
use std::num::ParseIntError;

pub mod playfield;
use playfield::Playfield;

fn main() -> Result<(), ParseIntError> {
    let matches = App::new("rustsweeper")
        .version("0.1.0")
        .author("Betül Ünlü <betulunlu0018@gmail.com>")
        .about("mines uwu idk lol")
        .arg(
            Arg::with_name("preset")
                .short("p")
                .long("preset")
                .value_name("DIFFICULTY")
                .possible_values(&["easy", "medium", "hard"])
                .conflicts_with("diff")
                .display_order(1)
                .next_line_help(true)
                .help("Preset difficulty"),
        )
        .arg(
            Arg::with_name("diff")
                .short("d")
                .long("difficulty")
                .value_names(&["WIDTH", "HEIGHT", "MINES"])
                .conflicts_with("preset")
                .next_line_help(true)
                .help("Set the difficulty, 3 integers"),
        )
        .get_matches();

    let mut state = Playfield::new(parse_args(matches)?);

    state.set_field();

    let duration = state.play();

    println!(
        "Game lasted {}.{} seconds",
        duration.as_secs(),
        duration.as_millis() % 1000
    );

    Ok(())
}

fn parse_args(matches: ArgMatches) -> Result<(usize, usize, usize), ParseIntError> {
    // if a preset flag is used
    if matches.is_present("preset") {
        match matches.value_of("preset").unwrap() {
            "easy" => Ok((8, 8, 10)),
            "medium" => Ok((16, 16, 40)),
            "hard" => Ok((30, 16, 99)),
            _ => unreachable!(),
        }
    } else if matches.is_present("diff") {
        let v = matches
            .values_of("diff")
            .unwrap()
            .map(|s| s.parse::<usize>())
            .collect::<Vec<_>>();

        for val in v.iter() {
            match val {
                Ok(_) => continue,
                Err(e) => return Err(e.to_owned()),
            }
        }

        Ok((
            v[0].clone().unwrap(),
            v[1].clone().unwrap(),
            v[2].clone().unwrap(),
        ))
    } else {
        Ok((8, 8, 10))
    }
}
