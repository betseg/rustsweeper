use std::fmt;
use std::time::{Duration, Instant};

pub mod tile;
use tile::{Mask, Tile, Val};

pub struct Playfield {
    pub field: Vec<Vec<Tile>>,
    pub width: usize,
    pub height: usize,
    pub mine_cnt: usize,
}

#[derive(PartialEq)]
enum Reason {
    Reveal,
    Explore,
}

impl Playfield {
    pub fn new((width, height, mine_cnt): (usize, usize, usize)) -> Self {
        Playfield {
            field: vec![
                vec![
                    Tile::new(
                        Val::Val(0),
                        if cfg!(debug_assertions) {
                            Mask::Visible
                        } else {
                            Mask::Hidden
                        }
                    );
                    width + 2
                ];
                height + 2
            ],
            width,
            height,
            mine_cnt,
        }
    }

    pub fn play(&mut self) -> Duration {
        let start = Instant::now();

        let result = loop {
            if self.is_solved() {
                break true;
            }

            println!("{}", self);

            match scanln_fmt_some!("{d} {d} {}\n", usize, usize, String) {
                (Some(x), Some(y), Some(ref f)) if f == "f" => {
                    if self.field[y][x].vsbl != Mask::Flag {
                        self.mine_cnt -= 1;
                    }
                    self.field[y][x].vsbl = Mask::Flag;
                }

                (Some(x), Some(y), Some(_)) | (Some(x), Some(y), None) => {
                    if self.field[y][x].is_mine() {
                        break false;
                    }

                    match self.field[y][x].vsbl {
                        Mask::Flag => {
                            self.mine_cnt += 1;
                        }
                        Mask::Visible => {
                            self.explore_neighbors(x, y, Reason::Reveal);
                        }
                        Mask::Hidden if self.field[y][x].val == Val::Val(0) => {
                            self.explore_neighbors(x, y, Reason::Explore);
                        }
                        _ => (),
                    };

                    self.field[y][x].vsbl = Mask::Visible;
                }

                _ => (),
            }
        };

        if result {
            println!("\nCongratulations! You've beaten the game!");
        } else {
            println!("\nBetter luck next time!");
        }

        for y in 1..=self.height {
            for x in 1..=self.width {
                self.field[y][x].vsbl = Mask::Visible;
            }
        }

        println!("{}", self);

        Instant::now().duration_since(start)
    }

    fn is_solved(&self) -> bool {
        let mut solved = 0;
        for y in 1..=self.height {
            for x in 1..=self.width {
                solved += (self.field[y][x].vsbl == Mask::Hidden) as usize;
            }
        }

        solved == self.mine_cnt
    }

    fn explore_neighbors(&mut self, x: usize, y: usize, reason: Reason) -> bool {
        use Reason::*;

        if !(1..=self.width).contains(&x) || !(1..=self.height).contains(&y) {
            return true;
        }
        if self.field[y][x].vsbl != Mask::Flag {
            self.field[y][x].vsbl = Mask::Visible;
        }

        let mut cnt = 0;

        for dy in 0..=2 {
            for dx in 0..=2 {
                if dx == 1 && dy == 1 {
                    continue;
                }
                match reason {
                    Explore => {
                        if self.field[y + dy - 1][x + dx - 1].is_mine() {
                            return false;
                        }
                    }
                    Reveal => {
                        cnt += self.field[y + dy - 1][x + dx - 1].is_flag() as usize;
                    }
                }
            }
        }

        match reason {
            Reveal if cnt != 0 => {
                for dy in 0..=2 {
                    for dx in 0..=2 {
                        if self.field[y + dy - 1][x + dx - 1].is_mine()
                            != self.field[y + dy - 1][x + dx - 1].is_flag()
                        {
                            return false;
                        }
                    }
                }
                if Val::Val(cnt) == self.field[y][x].val {
                    for dy in 0..=2 {
                        for dx in 0..=2 {
                            if dx == 1 && dy == 1 {
                                continue;
                            }
                            if !self.field[y + dy - 1][x + dx - 1].is_flag() {
                                self.field[y + dy - 1][x + dx - 1].vsbl = Mask::Visible;
                            }
                            if let Val::Val(0) = self.field[y + dy - 1][x + dx - 1].val {
                                self.explore_neighbors(x + dx - 1, y + dy - 1, Reason::Explore);
                            }
                        }
                    }
                }
            }

            Explore => {
                for dy in 0..=2 {
                    for dx in 0..=2 {
                        if dx == 1 && dy == 1 {
                            continue;
                        }
                        if let Mask::Hidden = self.field[y + dy - 1][x + dx - 1].vsbl {
                            self.explore_neighbors(x + dx - 1, y + dy - 1, Reason::Explore);
                        }
                    }
                }
            }
            _ => (),
        }

        true
    }

    pub fn set_field(&mut self) {
        extern crate rand;
        use rand::prelude::*;

        let mut rng = thread_rng();

        let mut i = 0;
        while i < self.mine_cnt {
            let (w, h) = (
                rng.gen_range(1, self.width + 1),
                rng.gen_range(1, self.height + 1),
            );
            if !self.field[h][w].is_mine() {
                self.field[h][w].val = Val::Mine;
                i += 1;
            }
        }

        for y in 1..=self.height {
            for x in 1..=self.width {
                if !self.field[y][x].is_mine() {
                    for dy in 0..=2 {
                        for dx in 0..=2 {
                            if dy == 1 && dx == 1 {
                                continue;
                            }
                            if self.field[y + dy - 1][x + dx - 1].is_mine() {
                                self.field[y][x].val += 1;
                            }
                        }
                    }
                }
            }
        }
    }
}

impl fmt::Display for Playfield {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use std::char;

        let mut out = if self.is_solved() {
            String::from(" ")
        } else {
            format!(
                "\n{} mine{} left\n ",
                self.mine_cnt,
                if self.mine_cnt == 1 { "" } else { "s" }
            )
        };

        for i in 1..=self.width {
            out.push(char::from_digit((i % 10) as u32, 10).unwrap());
        }
        out.push('\n');

        for i in 1..=self.height {
            out.push(char::from_digit((i % 10) as u32, 10).unwrap());
            for j in 1..=self.width {
                out.push_str(&format!("{}", self.field[i][j]));
            }
            out.push('\n');
        }

        write!(f, "{}", out)
    }
}
