use ansi_term::Colour::{Black, Blue, Cyan, Fixed, Green, Purple, Red, White};
use std::fmt;
use std::ops::AddAssign;

#[derive(Copy, Clone)]
pub struct Tile {
    pub val: Val,
    pub vsbl: Mask,
}

#[derive(Copy, Clone, PartialEq)]
pub enum Val {
    Val(usize),
    Mine,
}

#[derive(Copy, Clone, PartialEq)]
pub enum Mask {
    Hidden,
    Visible,
    Flag,
}

impl Tile {
    pub fn new(val: Val, vsbl: Mask) -> Self {
        Tile { val, vsbl }
    }

    pub fn is_mine(&self) -> bool {
        self.val == Val::Mine
    }

    pub fn is_flag(&self) -> bool {
        self.vsbl == Mask::Flag
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.vsbl {
            Mask::Hidden => write!(f, "{}", Fixed(8).paint(".")),
            Mask::Visible => write!(f, "{}", self.val),
            Mask::Flag => write!(f, "{}", Black.on(Red).paint("P")),
        }
    }
}

impl AddAssign<usize> for Val {
    fn add_assign(&mut self, rhs: usize) {
        match self {
            Val::Val(val) => *val += rhs,
            Val::Mine => (),
        }
    }
}

impl fmt::Display for Val {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let colors = vec![
            Fixed(8),
            Fixed(12),
            Green,
            Fixed(9),
            Blue,
            Cyan,
            Purple,
            White,
        ];
        match self {
            Val::Val(v) if *v == 1 => write!(
                f,
                "{}{}",
                colors[*v].bold().paint(v.to_string()),
                ansi_term::Style::default().paint(String::new())
            ),
            Val::Val(v) => write!(f, "{}", colors[*v].paint(v.to_string())),
            Val::Mine => write!(f, "{}", Black.on(Red).paint("O")),
        }
    }
}
